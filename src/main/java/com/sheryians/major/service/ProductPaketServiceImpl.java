package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.ProductPaket;
import com.sheryians.major.repository.ProductPaketRepo;



@Service
@Transactional
public class ProductPaketServiceImpl implements ProductPaketService {

	@Autowired
	private ProductPaketRepo productRepo;

	@Override
	public List<ProductPaket> getAll() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}

	@Override
	public ProductPaket save(ProductPaket product) {
		// TODO Auto-generated method stub
		return productRepo.save(product);
	}

	@Override
	public String delete(Long id) {
		// TODO Auto-generated method stub
		productRepo.deleteById(id);
		return "Delete Success !!!";
	}

	@Override
	public ProductPaket getProductById(Long id) {
		// TODO Auto-generated method stub
		return productRepo.findById(id).orElse(null);
	}

	@Override
	public ProductPaket updateProduct(ProductPaket product) {
		// TODO Auto-generated method stub
		ProductPaket oldProduct = null;
		Optional<ProductPaket> optProduct = productRepo.findById(product.getId());
		if (optProduct.isPresent()) {
			oldProduct = optProduct.get();
			oldProduct.setNamaProduct(product.getNamaProduct());
			oldProduct.setHarga(product.getHarga());
			oldProduct.setWaktu(product.getWaktu());
			oldProduct.setPaket(product.getPaket());
			productRepo.save(oldProduct);
		}else {
			return new ProductPaket();
		}
		return oldProduct;
	}
	
	

}
