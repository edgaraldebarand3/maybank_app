package com.sheryians.major.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.helper.CreditCardNumberGenerator;
import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.PembelianToken;
import com.sheryians.major.model.Rekening;
import com.sheryians.major.model.Token;
import com.sheryians.major.model.TokenPrice;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.PembelianTokenRepo;
import com.sheryians.major.repository.RekeningRepo;
import com.sheryians.major.repository.TokenRepo;



@Service
public class PembelianTokenServiceImpl implements PembelianTokenService{

	@Autowired
	private PembelianTokenRepo pembelianTokenRepo;
	
	@Autowired
	private TokenRepo tokenRepo;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Override
	public List<PembelianToken> getAll() {
		// TODO Auto-generated method stub
		return this.pembelianTokenRepo.findAll();
	}

	@Override
	public void save(PembelianToken pembelianToken, Token token, TokenPrice tokenPrice) {
		// TODO Auto-generated method stub
		
		//GET USER LOGIN DATA
		CurrentOnlineUser currentUser= Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		User user = users.get();
		
		//GET DATA REKENING USER
		Rekening rekening = this.rekeningRepo.findByNoRekening(user.getRekening().getNoRekening());
		Double saldoUser = rekening.getSaldo();
		
		//MENGURANGI SALDO SETELAH PEMBELIAN
		rekening.setSaldo(saldoUser - tokenPrice.getHargatoken());
		this.rekeningRepo.save(rekening);
		
		//GENERATE RANDOM TOKEN
		CreditCardNumberGenerator randomNumberToken = new CreditCardNumberGenerator();
		final String randomToken = randomNumberToken.generate("521", 10);
		
		//SET VALUE TOKEN ENTITY
		token.setCreatedDate(Date.valueOf(LocalDate.now()));
		token.setNoToken(randomToken);
		token.setTokenprices(tokenPrice);
		
		//SET VALUE PEMBELIANTOKEN ENTITY
		pembelianToken.setTanggalPembelian(Date.valueOf(LocalDate.now()));
		pembelianToken.setTokenpembelian(token);
		pembelianToken.setUser(user);
		
		//SAVE ALL
		this.tokenRepo.save(token);
		this.pembelianTokenRepo.save(pembelianToken);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.pembelianTokenRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.pembelianTokenRepo.findById(id);
	}

	@Override
	public Optional<PembelianToken> getPembelianTokenById(Long id) {
		// TODO Auto-generated method stub
		return this.pembelianTokenRepo.findById(id);
	}

	@Override
	public List<PembelianToken> findByUser(User user) {
		// TODO Auto-generated method stub
		return this.pembelianTokenRepo.findByUser(user);
	}

}
