package com.sheryians.major.service;

import java.util.List;

import com.sheryians.major.model.ProductPaket;


public interface ProductPaketService {
	
	public List<ProductPaket> getAll();
	public ProductPaket save(ProductPaket product);
	public String delete(Long id);
	public ProductPaket getProductById(Long id);
	public ProductPaket updateProduct(ProductPaket product);
	
}
