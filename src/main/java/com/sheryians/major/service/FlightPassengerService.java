package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.sheryians.major.model.FlightPassenger;


@Service
public interface FlightPassengerService {
	public List<FlightPassenger> getAll();
	public List<FlightPassenger> getSearch(String search, String sortField);
	public void save (FlightPassenger flightPassenger);
	public Optional<FlightPassenger> getFlightById(Long id);

}
