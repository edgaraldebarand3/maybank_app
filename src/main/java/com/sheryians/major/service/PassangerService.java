package com.sheryians.major.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sheryians.major.model.Passangers;


@Service
public interface PassangerService {
	public List<Passangers> getAll();
	public void save(Passangers passangers);
	
}
