package com.sheryians.major.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.sheryians.major.model.Token;
import com.sheryians.major.repository.TokenRepo;



public class TokenServiceImpl implements TokenService{

	@Autowired
	private TokenRepo tokenRepo;
	
	@Override
	public List<Token> getAll() {
		// TODO Auto-generated method stub
		return this.tokenRepo.findAll();
	}

	@Override
	public void save(Token token) {
		// TODO Auto-generated method stub
		this.tokenRepo.save(token);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.tokenRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.tokenRepo.findById(id);
	}

}
