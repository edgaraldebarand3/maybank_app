package com.sheryians.major.service;

import java.util.List;

import com.sheryians.major.model.ProductPaket;
import com.sheryians.major.model.TransferAmount;
import com.sheryians.major.model.User;


public interface TransferService {
	
	public List<TransferAmount> getAll();
	public List<TransferAmount> findByUser(User user);
	public TransferAmount save(TransferAmount amount, ProductPaket paket);
}
