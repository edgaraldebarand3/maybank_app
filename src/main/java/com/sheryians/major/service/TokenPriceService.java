package com.sheryians.major.service;

import java.util.List;

import com.sheryians.major.model.TokenPrice;



public interface TokenPriceService {

	public List<TokenPrice> getAll();
	public void save(TokenPrice tokenPrice);
	public void delete(Long id);
	public void getById(Long id);
}
