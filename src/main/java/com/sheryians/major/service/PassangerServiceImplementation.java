package com.sheryians.major.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.Passangers;
import com.sheryians.major.repository.PassangerRepositoy;



@Service
public class PassangerServiceImplementation implements PassangerService {
	
	@Autowired
	PassangerRepositoy passangerRepositoy;
	
	@Override
	public List<Passangers> getAll() {
		// TODO Auto-generated method stub
		return this.passangerRepositoy.findAll();
	}

	@Override
	public void save(Passangers passangers) {
		// TODO Auto-generated method stub
		this.passangerRepositoy.save(passangers);
	}

}
