package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.sheryians.major.model.AddFlight;


@Service
public interface AddFlightService {
	public List<AddFlight> getAll();
	public void save(AddFlight addFlight);


}
