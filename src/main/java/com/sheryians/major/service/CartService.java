package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import com.sheryians.major.model.Cart;
import com.sheryians.major.model.Product;
import com.sheryians.major.model.User;

public interface CartService {

	public List<Cart> getAll();
	public List<Cart> findByUserAndSold(User user, Boolean sold);
	public void save(Cart cart, User user, Product product);
	public void delete(Integer id);
	public void getById(Integer id);
	public Optional<Cart> getCartById(Integer id);
}
