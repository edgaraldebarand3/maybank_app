package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.Paket;
import com.sheryians.major.repository.PaketRepo;


@Service
@javax.transaction.Transactional
public class PaketServiceImpl implements PaketService {

	@Autowired
	private PaketRepo paketRepo;
	
	@Override
	public List<Paket> getAll() {
		// TODO Auto-generated method stub
		return paketRepo.findAll();
	}

	@Override
	public Paket save(Paket paket) {
		// TODO Auto-generated method stub
		return paketRepo.save(paket);
	}

	@Override
	public String delete(Long id) {
		// TODO Auto-generated method stub
		paketRepo.deleteById(id);
		return "Delete Success";
	}

	@Override
	public Paket getPaketById(Long id) {
		// TODO Auto-generated method stub
		return paketRepo.findById(id).orElse(null);
	}

	@Override
	public Paket updatePaket(Paket paket) {
		// TODO Auto-generated method stub
		Paket oldPaket = null;
		Optional<Paket> optPaket = paketRepo.findById(paket.getId());
		if (optPaket.isPresent()) {
			oldPaket = optPaket.get();
			oldPaket.setKouta(paket.getKouta());
			paketRepo.save(oldPaket);
		}else {
			return new Paket();
		}
		
		return oldPaket;
	}

	
}
