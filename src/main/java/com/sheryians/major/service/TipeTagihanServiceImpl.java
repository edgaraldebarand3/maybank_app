package com.sheryians.major.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.TipeTagihan;
import com.sheryians.major.repository.TipeTagihanRepo;



@Service
public class TipeTagihanServiceImpl implements TipeTagihanService{
	
	@Autowired
	private TipeTagihanRepo tipeTagihanRepo;

	
	
	@Override
	public List<TipeTagihan> getAll() {
		// TODO Auto-generated method stub
		return this.tipeTagihanRepo.findAll();
	}

	@Override
	public void save(TipeTagihan tipeTagihan) {
		// TODO Auto-generated method stub
		this.tipeTagihanRepo.save(tipeTagihan);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.tipeTagihanRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		
	}

}
