package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.sheryians.major.model.Pembayaran;
import com.sheryians.major.model.Tagihan;
import com.sheryians.major.model.User;



public interface PembayaranService {

	public Page<Pembayaran> getPaginateSearch(int pageNo, int pageSize, String field, String keyword);
	public Page<Pembayaran> getAllPaginate(int pageNo, int pageSize, String field);
	public List<Pembayaran> getAll();
	public void save(Pembayaran pembayaran,Tagihan tagihan);
	public void delete(Long id);
	public void getById(Long id);
	public Optional<Pembayaran> getPembayaranById(Long id);
	public List<Pembayaran> findByUser(User user);
}
