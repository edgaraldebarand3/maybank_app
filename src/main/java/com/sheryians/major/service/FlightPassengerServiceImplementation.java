package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.FlightPassenger;
import com.sheryians.major.repository.FlightPassengerRepository;



@Service
public class FlightPassengerServiceImplementation implements FlightPassengerService {
	
	@Autowired
	FlightPassengerRepository flightPassengerRepository;

	@Override
	public List<FlightPassenger> getAll() {
		// TODO Auto-generated method stub
		return this.flightPassengerRepository.findAll();
	}

//	@Override
//	public List<FlightPassenger> getSearch(String search) {
//		// TODO Auto-generated method stub
//		return this.flightPassengerRepository.findBySearch(search);
//		
//	}

	@Override
	public void save(FlightPassenger flightPassenger) {
		// TODO Auto-generated method stub
		this.flightPassengerRepository.save(flightPassenger);
	}

	@Override
	public List<FlightPassenger> getSearch(String search, String sortField) {
		// TODO Auto-generated method stub
		return this.flightPassengerRepository.findAll();
	}

	@Override
	public Optional<FlightPassenger> getFlightById(Long id) {
		// TODO Auto-generated method stub
		return this.flightPassengerRepository.findById(id);
	}

	

	

}
