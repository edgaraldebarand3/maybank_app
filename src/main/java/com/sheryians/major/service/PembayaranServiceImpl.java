package com.sheryians.major.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.Pembayaran;
import com.sheryians.major.model.Rekening;
import com.sheryians.major.model.Tagihan;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.PembayaranRepo;
import com.sheryians.major.repository.RekeningRepo;
import com.sheryians.major.repository.TagihanRepo;


@Service
public class PembayaranServiceImpl implements PembayaranService{

	@Autowired
	private PembayaranRepo pembayaranRepo;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private TagihanService tagihanService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TagihanRepo tagihanRepo;
	
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Override
	public List<Pembayaran> getAll() {
		// TODO Auto-generated method stub
		return this.pembayaranRepo.findAll();
	}

	@Override
	public void save(Pembayaran pembayaran, Tagihan tagihan) {
		
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser= Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		User user = users.get();
		//GET DATA REKENING USER
		Rekening rekening = this.rekeningRepo.findByNoRekening(user.getRekening().getNoRekening());
		Double saldoUser = rekening.getSaldo();
		
		//GET TAGIHAN USER
		Tagihan tagihanUser = this.tagihanRepo.findBynoPelanggan(tagihan.getNoPelanggan());
		tagihanUser.setPaid(true);
		Double tagihanAmount = tagihanUser.getHarga();
		
		//MENGURANGI SALDO SETELAH PEMBELIAN
		rekening.setSaldo(saldoUser - tagihanAmount);
		this.rekeningRepo.save(rekening);
		
		//SAVE PEMBAYARAN ENTITY
		pembayaran.setUser(user);
		pembayaran.setTagihanuser(tagihanUser);
		pembayaran.setTanggalPembayaran(Date.valueOf(LocalDate.now()));
		this.pembayaranRepo.save(pembayaran);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.pembayaranRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.pembayaranRepo.findById(id);
	}

	@Override
	public Optional<Pembayaran> getPembayaranById(Long id) {
		// TODO Auto-generated method stub
		return this.pembayaranRepo.findById(id);
	}

	@Override
	public Page<Pembayaran> getPaginateSearch(int pageNo, int pageSize, String field, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		
		return this.pembayaranRepo.search(keyword,paging);
	}

	@Override
	public Page<Pembayaran> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.pembayaranRepo.findAll(paging);
	}

	@Override
	public List<Pembayaran> findByUser(User user) {
		// TODO Auto-generated method stub
		return this.pembayaranRepo.findByUser(user);
	}

}
