package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.Cart;
import com.sheryians.major.model.Product;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.CartRepo;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
	private CartRepo cartRepo;
	
	@Override
	public List<Cart> getAll() {
		// TODO Auto-generated method stub
		return this.cartRepo.findAll();
	}

	@Override
	public List<Cart> findByUserAndSold(User user, Boolean sold) {
		// TODO Auto-generated method stub
		return this.cartRepo.findByUserAndSold(user, sold);
	}

	@Override
	public void save(Cart cart, User user, Product product) {
		// TODO Auto-generated method stub
		cart.setUser(user);
		cart.setProduct(product);
		this.cartRepo.save(cart);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		this.cartRepo.deleteById(id);
	}

	@Override
	public void getById(Integer id) {
		// TODO Auto-generated method stub
		this.cartRepo.findById(id);
	}

	@Override
	public Optional<Cart> getCartById(Integer id) {
		// TODO Auto-generated method stub
		return this.cartRepo.findById(id);
	}

}
