package com.sheryians.major.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.Order;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	OrderRepository orderRepository;
	
	public List<Order> getAllOrder(){
		return orderRepository.findAll();
	}
	
	public void addOrder(Order order) {
		orderRepository.save(order);
	}
	
	public List<Order> findByUser(User user){
		return orderRepository.findByUser(user);
	}
}
