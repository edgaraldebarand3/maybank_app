package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.ProductPaket;
import com.sheryians.major.model.Rekening;
import com.sheryians.major.model.TransferAmount;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.RekeningRepo;
import com.sheryians.major.repository.TransferRepo;



@Service
@Transactional
public class TransferServiceImpl implements TransferService  {
	
	@Autowired
	private TransferRepo repo;
	
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Autowired
	private UserService userService;

	@Override
	public List<TransferAmount> getAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public TransferAmount save(TransferAmount amount, ProductPaket paket) {
		// TODO Auto-generated method stub
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser= Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		User user = users.get();
		
		//GET DATA REKENING USER
		Rekening rekening = this.rekeningRepo.findByNoRekening(user.getRekening().getNoRekening());
		Double saldoUser = rekening.getSaldo();
		
		//MENGURANGI SALDO SETELAH PEMBELIAN
		rekening.setSaldo(saldoUser - paket.getHarga());
		this.rekeningRepo.save(rekening);
		return repo.save(amount);
	}

	@Override
	public List<TransferAmount> findByUser(User user) {
		// TODO Auto-generated method stub
		return this.repo.findByUser(user);
	}

}
