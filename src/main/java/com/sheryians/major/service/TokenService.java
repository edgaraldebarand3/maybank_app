package com.sheryians.major.service;

import java.util.List;

import com.sheryians.major.model.Token;



public interface TokenService {

	public List<Token> getAll();
	public void save(Token token);
	public void delete(Long id);
	public void getById(Long id);
}
