package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.Tagihan;
import com.sheryians.major.model.TipeTagihan;
import com.sheryians.major.repository.TagihanRepo;



@Service
public class TagihanServiceImpl implements TagihanService{

	@Autowired
	private TagihanRepo tagihanRepo;
	
	@Override
	public List<Tagihan> getAll() {
		// TODO Auto-generated method stub
		return this.tagihanRepo.findAll();
	}

	@Override
	public void save(Tagihan tagihan, TipeTagihan tipeTagihan) {
		// TODO Auto-generated method stub
		tagihan.setTipetagihan(tipeTagihan);
		this.tagihanRepo.save(tagihan);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.tagihanRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.tagihanRepo.findById(id);
	}

	@Override
	public Tagihan findBynoPelanggan(String noPelanggan) {
		// TODO Auto-generated method stub
		return this.tagihanRepo.findBynoPelanggan(noPelanggan);
	}

	@Override
	public Optional<Tagihan> getPembayaranById(Long id) {
		// TODO Auto-generated method stub
		return this.tagihanRepo.findById(id);
	}

	@Override
	public Page<Tagihan> getPaginateSearch(int pageNo, int pageSize, String field, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		
		return this.tagihanRepo.search(keyword,paging);
	}

	@Override
	public Page<Tagihan> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.tagihanRepo.findAll(paging);
	}

}
