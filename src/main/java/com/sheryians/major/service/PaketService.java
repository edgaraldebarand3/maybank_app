package com.sheryians.major.service;

import java.util.List;

import com.sheryians.major.model.Paket;


public interface PaketService {

	public List<Paket> getAll();
	public Paket save(Paket paket);
	public String delete(Long id);
	public Paket getPaketById(Long id);
	public Paket updatePaket(Paket paket);
}
