package com.sheryians.major.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.TokenPrice;
import com.sheryians.major.repository.TokenPriceRepo;


@Service
public class TokenPriceServiceImpl implements TokenPriceService{
	
	@Autowired
	private TokenPriceRepo tokenPriceRepo;

	@Override
	public List<TokenPrice> getAll() {
		// TODO Auto-generated method stub
		return this.tokenPriceRepo.findAll();
	}

	@Override
	public void save(TokenPrice tokenPrice) {
		// TODO Auto-generated method stub
		this.tokenPriceRepo.save(tokenPrice);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.tokenPriceRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.tokenPriceRepo.findById(id);
	}

}
