package com.sheryians.major.service;

import java.util.List;

import com.sheryians.major.model.TipeTagihan;



public interface TipeTagihanService {

	public List<TipeTagihan> getAll();
	public void save(TipeTagihan tipeTagihan);
	public void delete(Long id);
	public void getById(Long id);
}
