package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.CustomUserDetails;
import com.sheryians.major.model.Role;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Optional<User> user = userRepository.findUserByEmail(email);
		Long rek_id;
		
		String username = user.get().getFirstName();
		int id = user.get().getId();
		String emailku = user.get().getEmail();
		
		List<Role> roles = user.get().getRoles();
		if(roles.size()>0) {
			for(Role role:roles) {
				if(role.getName().equals("ROLE_USER")) {
					rek_id = user.get().getRekening().getId();
					CurrentOnlineUser userData = new CurrentOnlineUser(id, username, emailku, rek_id);
					Prevalent.currentOnlineUser = userData;
				}
				else {
					CurrentOnlineUser userData = new CurrentOnlineUser(id, username, emailku);
					Prevalent.currentOnlineUser = userData;
				}
			}
		}
		user.orElseThrow(() -> new UsernameNotFoundException("User Not Found"));
		return user.map(CustomUserDetails::new).get();
	}

	
}
