package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.sheryians.major.model.Tagihan;
import com.sheryians.major.model.TipeTagihan;




public interface TagihanService {

	public Page<Tagihan> getPaginateSearch(int pageNo, int pageSize, String field, String keyword);
	public Page<Tagihan> getAllPaginate(int pageNo, int pageSize, String field);
	public List<Tagihan> getAll();
	public void save(Tagihan tagihan, TipeTagihan tipeTagihan);
	public void delete(Long id);
	public void getById(Long id);
	public Tagihan findBynoPelanggan(String noPelanggan);
	public Optional<Tagihan> getPembayaranById(Long id);
}
