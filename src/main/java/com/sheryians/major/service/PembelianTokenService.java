package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import com.sheryians.major.model.PembelianToken;
import com.sheryians.major.model.Token;
import com.sheryians.major.model.TokenPrice;
import com.sheryians.major.model.User;


public interface PembelianTokenService {

	public List<PembelianToken> getAll();
	public List<PembelianToken> findByUser(User user);
	public void save(PembelianToken pembelianToken, Token token, TokenPrice tokenPrice);
	public void delete(Long id);
	public void getById(Long id);
	public Optional<PembelianToken> getPembelianTokenById(Long id);
}
