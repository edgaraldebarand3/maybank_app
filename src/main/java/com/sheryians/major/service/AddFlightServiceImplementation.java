package com.sheryians.major.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.AddFlight;
import com.sheryians.major.repository.AddFlightRepository;



@Service
public class AddFlightServiceImplementation implements AddFlightService{
	
	@Autowired
	AddFlightRepository addFlightRepository;

	@Override
	public List<AddFlight> getAll() {
		// TODO Auto-generated method stub
		return this.addFlightRepository.findAll();
	}

	@Override
	public void save(AddFlight addFlight) {
		// TODO Auto-generated method stub
		this.addFlightRepository.save(addFlight);
	}



	


	
	
	
	

}
