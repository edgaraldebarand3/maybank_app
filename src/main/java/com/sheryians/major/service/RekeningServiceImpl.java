package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.Rekening;
import com.sheryians.major.repository.RekeningRepo;


@Service
public class RekeningServiceImpl implements RekeningService{
	
	@Autowired
	private RekeningRepo rekeningRepo;

	@Override
	public List<Rekening> getAll() {
		// TODO Auto-generated method stub
		
		return this.rekeningRepo.findAll();
	}

	@Override
	public void save(Rekening rekening) {
		// TODO Auto-generated method stub
		this.rekeningRepo.save(rekening);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.rekeningRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.rekeningRepo.findById(id);
	}

	@Override
	public Rekening findByNoRekening(String noRekening) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findByNoRekening(noRekening);
	}

	@Override
	public Optional<Rekening> getRekeningById(Long id) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findById(id);
	}

}
