package com.sheryians.major.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sheryians.major.model.Role;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.RoleRepository;
import com.sheryians.major.repository.UserRepository;

@Service
@Transactional
public class InitDefaultUserAuth {
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@PostConstruct
	public void index() {
		//create Role
		Role roleAdmin = new Role();
		Role roleUser = new Role();
		
		roleAdmin.setName("ROLE_ADMIN");;
		roleUser.setName("ROLE_USER");
		//save Role
		this.roleRepository.save(roleAdmin);
		this.roleRepository.save(roleUser);
		
		
		//Create User
		List<Role> RoleAdmin = new ArrayList<>();
		
		RoleAdmin.add(roleAdmin);
		
		User userAdmin = new User();
		
		userAdmin.setFirstName("admin");
		userAdmin.setEmail("admin@gmail.com");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("admin"));
		userAdmin.setRoles(RoleAdmin);
		
		//SAVE USER
		this.userRepository.save(userAdmin);
	}

}
