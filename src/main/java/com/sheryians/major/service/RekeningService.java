package com.sheryians.major.service;

import java.util.List;
import java.util.Optional;

import com.sheryians.major.model.Rekening;



public interface RekeningService {

	public List<Rekening> getAll();
	public void save(Rekening rekening);
	public void delete(Long id);
	public void getById(Long id);
	public Rekening findByNoRekening(String noRekening);
	public Optional<Rekening> getRekeningById(Long id);
}
