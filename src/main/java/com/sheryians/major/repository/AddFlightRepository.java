package com.sheryians.major.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.AddFlight;


public interface AddFlightRepository extends JpaRepository<AddFlight, Long>{
	

}
