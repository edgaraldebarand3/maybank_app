package com.sheryians.major.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.TipeTagihan;


public interface TipeTagihanRepo extends JpaRepository<TipeTagihan, Long>{

}
