package com.sheryians.major.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.ProductPaket;


public interface ProductPaketRepo extends JpaRepository<ProductPaket, Long> {

}
