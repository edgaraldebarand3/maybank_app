package com.sheryians.major.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sheryians.major.model.Tagihan;


public interface TagihanRepo extends JpaRepository<Tagihan, Long>{

	@Query(value = "SELECT * FROM tagihan t WHERE t.name iLIKE %:keyword%" 
	+ " OR t.no_pelanggan iLike %:keyword%", nativeQuery = true)
	Page<Tagihan> search(@Param("keyword") String keyword, Pageable pageable);
	Tagihan findBynoPelanggan(String noPelanggan);
}
