package com.sheryians.major.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sheryians.major.model.FlightPassenger;


public interface FlightPassengerRepository extends JpaRepository<FlightPassenger, Long> {
	@Query(value = "SELECT * FROM flightschedule s WHERE s.Asal iLIKE %:search%"
			+ " OR s.Tujuan iLIKE %:search%"
			+ " OR s.tanggalKeberangkatan iLIKE %:search%", nativeQuery = true)
	List<FlightPassenger> findBySearch(@Param("search") String search);
	
	
	
}
