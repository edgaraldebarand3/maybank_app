package com.sheryians.major.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.Rekening;


public interface RekeningRepo extends JpaRepository<Rekening, Long>{

	Rekening findByNoRekening(String noRekening);
}
