package com.sheryians.major.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.Passangers;


public interface PassangerRepositoy extends JpaRepository<Passangers, Long> {

}
