package com.sheryians.major.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.TransferAmount;
import com.sheryians.major.model.User;


public interface TransferRepo extends JpaRepository<TransferAmount, Long>{

	List<TransferAmount> findByUser(User user);
}
