package com.sheryians.major.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.Cart;
import com.sheryians.major.model.User;

public interface CartRepo extends JpaRepository<Cart, Integer>{

	List<Cart> findByUserAndSold(User user, Boolean sold);
}
