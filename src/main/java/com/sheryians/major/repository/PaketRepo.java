package com.sheryians.major.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.Paket;


public interface PaketRepo extends JpaRepository<Paket, Long>{

}
