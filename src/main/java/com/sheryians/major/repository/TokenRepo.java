package com.sheryians.major.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.Token;


public interface TokenRepo extends JpaRepository<Token, Long>{

}
