package com.sheryians.major.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheryians.major.model.PembelianToken;
import com.sheryians.major.model.User;



public interface PembelianTokenRepo extends JpaRepository<PembelianToken, Long>{

	List<PembelianToken> findByUser(User user);
}
