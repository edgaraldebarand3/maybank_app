package com.sheryians.major.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sheryians.major.model.Pembayaran;
import com.sheryians.major.model.User;



public interface PembayaranRepo extends JpaRepository<Pembayaran, Long>{

	@Query(value = "SELECT * FROM pembayaran p WHERE p.tanggal_pembayaran iLIKE %:keyword%", nativeQuery = true)
	Page<Pembayaran> search(@Param("keyword") String keyword, Pageable pageable);
	List<Pembayaran> findByUser(User user);
}
