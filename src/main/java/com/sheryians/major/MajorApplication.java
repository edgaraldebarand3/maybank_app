package com.sheryians.major;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sheryians.major.model.TipeTagihan;
import com.sheryians.major.model.TokenPrice;
import com.sheryians.major.repository.TipeTagihanRepo;
import com.sheryians.major.repository.TokenPriceRepo;

@SpringBootApplication
public class MajorApplication implements ApplicationRunner{
	
	@Autowired
	private TokenPriceRepo tokenPriceRepo;
	
	@Autowired
	private TipeTagihanRepo tipeTagihanRepo;

	public static void main(String[] args) {
		SpringApplication.run(MajorApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		//TOKEN PRICE
		TokenPrice tokenPrice = new TokenPrice();
		TokenPrice tokenPrice1 = new TokenPrice();
		TokenPrice tokenPrice2 = new TokenPrice();
		TokenPrice tokenPrice3 = new TokenPrice();
				
		tokenPrice.setHargatoken(100000.0);
		tokenPrice1.setHargatoken(250000.0);
		tokenPrice2.setHargatoken(500000.0);
		tokenPrice3.setHargatoken(1000000.0);
				
		//TIPE TAGIHAN
		TipeTagihan tagihan = new TipeTagihan();
		TipeTagihan tagihan1 = new TipeTagihan();
				
		tagihan.setNamatipe("Listrik");
		tagihan1.setNamatipe("Internet");
				
		List<TokenPrice> tokenPrices = Arrays.asList(tokenPrice,tokenPrice1,tokenPrice2,tokenPrice3);
		List<TipeTagihan> tipeTagihans = Arrays.asList(tagihan,tagihan1);
		tokenPriceRepo.saveAll(tokenPrices);
		tipeTagihanRepo.saveAll(tipeTagihans);
	}

}
