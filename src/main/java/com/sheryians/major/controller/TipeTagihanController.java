package com.sheryians.major.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sheryians.major.model.TipeTagihan;
import com.sheryians.major.service.TipeTagihanService;


@Controller
@RequestMapping("/admin/tipetagihan")
public class TipeTagihanController {

	
	@Autowired
	private TipeTagihanService tipeTagihanService;
	
	@GetMapping
	public String index(Model model) {
		List<TipeTagihan> tipetagihan;
		
		
		tipetagihan = this.tipeTagihanService.getAll();
		model.addAttribute("tipeTagihanForm", new TipeTagihan());
		model.addAttribute("page", tipetagihan);
		
		return "tipetagihan";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("tipeTagihanForm") TipeTagihan tipeTagihan,
			RedirectAttributes redirectAttributes, Model model, BindingResult result) {
		if(result.hasErrors()) {
			List<TipeTagihan> tipeTagihan1 = this.tipeTagihanService.getAll();
			model.addAttribute("page",tipeTagihan1);
			
			return "tipetagihan";
		}
		this.tipeTagihanService.save(tipeTagihan);
		redirectAttributes.addFlashAttribute("Success","data inserted");
		return "redirect:/admin/tipetagihan";
	}
	
	@GetMapping("/delete")
	public String delete(TipeTagihan tipeTagihan, RedirectAttributes redirectAttributes) {
		this.tipeTagihanService.delete(tipeTagihan.getId());
		return "redirect:/admin/tipetagihan";
	}
}
