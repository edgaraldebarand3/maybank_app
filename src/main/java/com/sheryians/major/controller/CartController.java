package com.sheryians.major.controller;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.sheryians.major.dto.ProductDTO;
import com.sheryians.major.global.Cart;
import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.Order;
import com.sheryians.major.model.Product;
import com.sheryians.major.model.Rekening;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.RekeningRepo;
import com.sheryians.major.service.CartService;
import com.sheryians.major.service.OrderService;
import com.sheryians.major.service.ProductService;
import com.sheryians.major.service.RekeningService;
import com.sheryians.major.service.UserService;

@Controller
public class CartController {

	@Autowired
	ProductService productService;
	@Autowired
	OrderService orderService;
	@Autowired
	UserService userService;
	@Autowired
	private RekeningRepo rekeningRepo;
	@Autowired
	private RekeningService rekeningService;
	@Autowired
	private CartService cartService;
	
	@GetMapping("/addToCart/{id}")
	public String addToCart(@PathVariable Long id, com.sheryians.major.model.Cart cart) {
		
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		
		//GET DATA PRODUCT
		Product product = productService.getProductById(id).get();
		
		Cart.cart.add(productService.getProductById(id).get());
		cartService.save(cart, users.get(), product);
		return "redirect:/shop";
	}

	@GetMapping("/cart")
	public String cart(Model model) {
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		
		Boolean sold = false;
		Double total = 0.0;
		
		//GET DATA CART
		List<com.sheryians.major.model.Cart> carts  = this.cartService.findByUserAndSold(users.get(), sold);
		
		for (com.sheryians.major.model.Cart cart : carts) {
			total += cart.getProduct().getPrice();
		}
		
		model.addAttribute("cartCount", carts.size());
		model.addAttribute("total", total);
		model.addAttribute("cart", carts);
		
		return "cart";
	}

	@GetMapping("/cart/removeItem/{index}")
	public String removeCart(@PathVariable int index) {
		Cart.cart.remove(index);
		return "redirect:/cart";
	}

	@GetMapping("/checkout")
	public String checkout(Model model) {
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
				
		Double total = 0.0;
		Boolean sold = false;
				
		//GET DATA CART
		List<com.sheryians.major.model.Cart> carts  = this.cartService.findByUserAndSold(users.get(), sold);
				
		for (com.sheryians.major.model.Cart cart : carts) {
			total += cart.getProduct().getPrice();
		}
		
		model.addAttribute("orderForm", new Order());
		model.addAttribute("total", total);
		return "checkout";
	}
	
	  @PostMapping("/checkout/add") 
	  public String postOrderAdd(@ModelAttribute("orderForm") Order order) { 
		  
		  Double total = 0.0;
		  
		  //GET DATA USER LOGIN
		  CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		  Optional<User> users = userService.getUserById(currentUser.getId());
		  
		  //GET DATA REKENING USER
		  Rekening rekening = this.rekeningRepo.findByNoRekening(users.get().getRekening().getNoRekening());
		  Double saldoUser = rekening.getSaldo();
		  
		  //MENGURANGI SALDO USER
		  rekening.setSaldo(saldoUser - Cart.cart.stream().mapToDouble(Product::getPrice).sum());
		  rekeningService.save(rekening);
		  
		  //GET CART BY USER ID
		  Boolean sold = false;
		  List<com.sheryians.major.model.Cart> carts  = this.cartService.findByUserAndSold(users.get(), sold);
		  
		  //IF USER ORDER, CHANGE STATUS SOLD IN CART TO TRUE  
		  for (com.sheryians.major.model.Cart cart : carts) {
			cart.setSold(true);
			total += cart.getProduct().getPrice();
		}
		  
		  order.setCarts(Cart.cart.stream().mapToDouble(Product::getPrice).sum());
		  User user1 = users.get();
		  order.setUser(user1);
		  orderService.addOrder(order); 
		  return "redirect:/history"; 
		}
	  
	  @GetMapping("/history") public String getOrderAdd(Model model) {
		  
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		  
		model.addAttribute("order", orderService.findByUser(users.get())); 
		
		return "orderPlaced"; 
		  
	  }
	 
}
