package com.sheryians.major.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sheryians.major.model.Passangers;
import com.sheryians.major.service.PassangerService;


@Controller
@RequestMapping("/passanger")
public class PassangerController {

	@Autowired
	private PassangerService passangerService;
	
	@GetMapping
	public String index (String keyword, 
			
			Model model) {
		List<Passangers> passangers1 = this.passangerService.getAll();
		
		model.addAttribute("passangerForm", new Passangers());
		model.addAttribute("passangers",passangers1);
		return "passanger";
	}
	
	@PostMapping("/save")
	public String save (@ModelAttribute("passangerForm") Passangers passanger,
			BindingResult result,
			RedirectAttributes redirectAttributes,
			Model model) {
		System.out.println("Nama Lengkap"+passanger.getPassangerName());
		passanger.setPassangerName(passanger.getPassangerName());
		this.passangerService.save(passanger);
        redirectAttributes.addFlashAttribute("Data Berhasil diinput");
        return "redirect:/flightpassenger";
	}
}
