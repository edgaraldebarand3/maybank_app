package com.sheryians.major.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.token.TokenService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sheryians.major.model.TokenPrice;
import com.sheryians.major.service.TokenPriceService;



@Controller
@RequestMapping("admin/token")
public class TokenPriceController {

	@Autowired
	private TokenPriceService tokenPriceService;
	
	@GetMapping
	public String index(Model model) {
		List<TokenPrice> tipetoken;
		
		
		tipetoken = this.tokenPriceService.getAll();
		model.addAttribute("tipeTokenForm", new TokenPrice());
		model.addAttribute("page", tipetoken);
		
		return "tokenprice";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("tipeTokenForm") TokenPrice tokenPrice,
			RedirectAttributes redirectAttributes, Model model, BindingResult result) {
		if(result.hasErrors()) {
			List<TokenPrice> tipeToken = this.tokenPriceService.getAll();
			model.addAttribute("page",tipeToken);
			
			return "tokenprice";
		}
		this.tokenPriceService.save(tokenPrice);
		redirectAttributes.addFlashAttribute("Success","data inserted");
		return "redirect:/admin/token/tokenprice";
	}
	
	@GetMapping("/delete")
	public String delete(TokenPrice tokenPrice, RedirectAttributes redirectAttributes) {
		this.tokenPriceService.delete(tokenPrice.getId());
		return "redirect:/admin/token/tokenprice";
	}
}
