package com.sheryians.major.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.ProductPaket;
import com.sheryians.major.model.TransferAmount;
import com.sheryians.major.model.User;
import com.sheryians.major.service.ProductPaketService;
import com.sheryians.major.service.TransferService;
import com.sheryians.major.service.UserService;


@Controller
@RequestMapping("/user")
public class TransaksiApi {

	@Autowired
	private ProductPaketService productService;
	
	@Autowired
	private TransferService transferService;
	
	@Autowired UserService userService;
	
	@GetMapping("/transfer")
	public String Transfer(Model model) {
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		
		List<TransferAmount> listTransfer = transferService.findByUser(users.get());
		model.addAttribute("listTransfer", listTransfer);
		return "transaksi";
	}
	
	
	@GetMapping("/addTransferPage")
	public String newTransfer(Model model) {
		model.addAttribute("transfer_amount", new TransferAmount());
		model.addAttribute("product", new ProductPaket());
		
		List<ProductPaket> product = productService.getAll();
		model.addAttribute("products", product);
		
		return "form_transaksi";
		
	}
	
	@PostMapping("/addSaveTransfer")
	public String saveTransaksi(@Valid @ModelAttribute("transaksi") TransferAmount transferAmount,
			@Valid @ModelAttribute("product") ProductPaket product,
			RedirectAttributes attributes,
			Model model, BindingResult result) {
		if (result.hasErrors()) {
			return "transaksi";
		}	
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		
		
		//SET VALUE TRANSFER
		transferAmount.setUser(users.get());
		transferAmount.setProduct(product);
		transferService.save(transferAmount, product);
		attributes.addFlashAttribute("success", "Paket Berhasil di beli");
			
	
		return "redirect:/user/transfer";
	}
	
	@GetMapping("/detail")
	public String detail(@RequestParam("id") Long id, Model model) {
		ProductPaket product = productService.getProductById(id);
		model.addAttribute("list_details", product);
		return "redirect:/user/detail_pengirim";
	}
}
