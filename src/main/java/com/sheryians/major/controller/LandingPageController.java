package com.sheryians.major.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.sheryians.major.global.Cart;
import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.User;
import com.sheryians.major.service.CartService;
import com.sheryians.major.service.CategoryService;
import com.sheryians.major.service.ProductService;
import com.sheryians.major.service.UserService;

@Controller
public class LandingPageController {

	@Autowired
	CategoryService categoryService;
	@Autowired
	ProductService productService;
	@Autowired
	UserService userService;
	@Autowired
	private CartService cartService;
	
	@GetMapping({"/", "/home"})
	public String index(Model model) {
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		
		
		if(currentUser != null) {
		Optional<User> users = userService.getUserById(currentUser.getId());
		Boolean sold = false;
			
		//GET DATA CART
		List<com.sheryians.major.model.Cart> carts  = this.cartService.findByUserAndSold(users.get(), sold);
		model.addAttribute("cartCount", carts.size());
		}else {
			model.addAttribute("cartCount", Cart.cart.size());
		}
		return "index";
	}
	
	@GetMapping("/shop")
	public String shop(Model model) {
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
						
		Boolean sold = false;
						
		//GET DATA CART
		List<com.sheryians.major.model.Cart> carts  = this.cartService.findByUserAndSold(users.get(), sold);
		
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("products", productService.getAllProduct());
		model.addAttribute("cartCount", carts.size());
		return "shop";
	}
	
	@GetMapping("/shop/category/{id}")
	public String shopByCategory(Model model, @PathVariable int id) {
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
								
		Boolean sold = false;
								
		//GET DATA CART
		List<com.sheryians.major.model.Cart> carts  = this.cartService.findByUserAndSold(users.get(), sold);
		
		model.addAttribute("categories", categoryService.getAllCategory());
		model.addAttribute("cartCount", carts.size());
		model.addAttribute("products", productService.getAllProductsByCategoryId(id));
		return "shop";
	}
	
	@GetMapping("/shop/viewproduct/{id}")
	public String viewProduct(Model model, @PathVariable Long id) {
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
										
		Boolean sold = false;
										
		//GET DATA CART
		List<com.sheryians.major.model.Cart> carts  = this.cartService.findByUserAndSold(users.get(), sold);
		
		model.addAttribute("product", productService.getProductById(id).get());
		model.addAttribute("cartCount", carts.size());
		return "viewProduct";
	}
	
}
