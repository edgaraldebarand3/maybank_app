package com.sheryians.major.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sheryians.major.model.Paket;
import com.sheryians.major.service.PaketService;


@Controller
@RequestMapping("/admin")
public class PaketApi {
	
	@Autowired
	PaketService paketService;
	
	@GetMapping("/paket")
	public String paket(Model model) {
		List<Paket> listPaket = paketService.getAll();
		model.addAttribute("pakets", listPaket);
		return "bank";
	}
	
	@GetMapping("/addPaketPage")
	public String newPaket(Model model) {
		model.addAttribute("paket", new Paket());
		return "form_bank";
	}
	
	@PostMapping("/addSavePaket")
	public String savePaket(Paket paket) {
		paketService.save(paket);
		return "redirect:/admin/paket";
	}
	
	  @GetMapping("/{id}") 
	  public String edit(@PathVariable("id") Long id, Model
	  model) { 
		  Paket paket = paketService.getPaketById(id);
		  model.addAttribute("paket", paket); 
		  return "form_bank"; 
	  }
	 
	
	@GetMapping("/deletePaket/{id}")
	public String delete(@PathVariable("id") Long id, Model model) {
		paketService.delete(id);
		return "redirect:/admin/paket";
	}
}
