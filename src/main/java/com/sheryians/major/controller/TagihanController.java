package com.sheryians.major.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sheryians.major.model.Tagihan;
import com.sheryians.major.model.TipeTagihan;
import com.sheryians.major.service.TagihanService;
import com.sheryians.major.service.TipeTagihanService;


@Controller
@RequestMapping("/admin/tagihan")
public class TagihanController {

	@Autowired
	private TagihanService tagihanService;
	
	@Autowired
	private TipeTagihanService tipeTagihanService;
	
	@GetMapping
	public String index(
			@RequestParam(required = false) String keyword,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			Model model) {
		Page<Tagihan> tagihan;
		List<TipeTagihan> tipeTagihan = this.tipeTagihanService.getAll();
		
		if(keyword == null) {
			tagihan = this.tagihanService.getAllPaginate(pageNo, pageSize, sortField);
		}else {
			tagihan = this.tagihanService.getPaginateSearch(pageNo, pageSize, sortField, keyword);
		}
		model.addAttribute("keyword", keyword);
		model.addAttribute("tagihanForm", new Tagihan());
		model.addAttribute("page", tagihan);
		model.addAttribute("tipeTagihan",tipeTagihan);
		
		return "tagihan";
	}
	
	@GetMapping("/save")
	public String saveTagihan(Model model) {
		model.addAttribute("daftartagihan", this.tagihanService.getAll());
		model.addAttribute("tipeTagihan", this.tipeTagihanService.getAll());
		model.addAttribute("tagihanForm", new Tagihan());
		return "tagihanAdd";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("tagihanForm") Tagihan tagihan,
			@Valid @ModelAttribute("tipeTagihan") TipeTagihan tipeTagihan,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			RedirectAttributes redirectAttributes, Model model, BindingResult result) {
		if(result.hasErrors()) {
			Page<Tagihan> tagihan1 = this.tagihanService.getAllPaginate(pageNo, pageSize, sortField);
			model.addAttribute("page",tagihan1);
			
			return "tagihan";
		}
		this.tagihanService.save(tagihan, tipeTagihan);
		redirectAttributes.addFlashAttribute("Success","data inserted");
		return "redirect:/admin/tagihan";
	}
	
	@GetMapping("/delete")
	public String delete(Tagihan tagihan, RedirectAttributes redirectAttributes) {
		this.tagihanService.delete(tagihan.getId());
		return "redirect:/admin/tagihan";
	}
	
}
