package com.sheryians.major.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sheryians.major.model.Paket;
import com.sheryians.major.model.Product;
import com.sheryians.major.model.ProductPaket;
import com.sheryians.major.service.PaketService;
import com.sheryians.major.service.ProductPaketService;


@Controller
@RequestMapping("/admin")
public class ProductApi {
	
	@Autowired
	private ProductPaketService productService;
	
	
	@Autowired
	private PaketService paketService;
	
	
	@GetMapping("/product")
	public String rekening(Model model) {
		List<ProductPaket> listProduct = productService.getAll();
		model.addAttribute("products", listProduct);
		return "rekening";
	}
	
	@GetMapping("/addProductPage")
	public String newRekening(Model model) {
		
		model.addAttribute("product", new ProductPaket());
		
		List<Paket> paket = paketService.getAll();
		model.addAttribute("pakets", paket);
		return "form_rekening";
	}
	
	@PostMapping("/addSaveProduct")
	public String saveRekening(
			@Valid @ModelAttribute("product") ProductPaket product,
			@Valid @ModelAttribute("paket") Paket paket,
			Model model, BindingResult result) {
		if (result.hasErrors()) {
			return "rekening";
		}

		product.setPaket(paket);
		productService.save(product);
		return "redirect:/admin/product";
	}

	
	 @GetMapping("/edit/{id}") 
	 public String edit(@PathVariable("id") Long id, Model model) { 
		 ProductPaket product = productService.getProductById(id);
		 List<Paket> paket = paketService.getAll();
		 model.addAttribute("pakets", paket);
		 model.addAttribute("product", product); 
		 
		 return "form_rekening"; 
		 }
	 
	@GetMapping("/deleteProduct/{id}")
	public String delete(@PathVariable("id") Long id, Model model) {
		productService.delete(id);
		return "redirect:/admin/product";
	}
}
