package com.sheryians.major.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sheryians.major.model.AddFlight;
import com.sheryians.major.model.FlightPassenger;
import com.sheryians.major.service.AddFlightService;
import com.sheryians.major.service.FlightPassengerService;



@Controller
@RequestMapping("/flightpassenger")
public class FlightPassengerController {
	
	@Autowired
	FlightPassengerService flightPassengerService;
	
	@Autowired
	AddFlightService addFlightService;
	
	@GetMapping
	public String index (String keyword, 
			@RequestParam (value = "search", defaultValue = "") String search,
			@RequestParam(value="sortField", defaultValue = "id") String sortField,
			Model model) {
		List<FlightPassenger> flightpass1;
		List<AddFlight> addflight1 = this.addFlightService.getAll();
		if (search != null) {
			flightpass1 = this.flightPassengerService.getSearch(search, sortField);

		}else {
			flightpass1 = this.flightPassengerService.getAll();
		}

		model.addAttribute("flightpassForm", new FlightPassenger());
		model.addAttribute("addflightForm", new AddFlight());
		model.addAttribute("search",search);
		model.addAttribute("flightpass1",flightpass1);
		model.addAttribute("addflight1",addflight1);
		return "flightpassenger";
	}
	
	@PostMapping("/save")
	public String save (@ModelAttribute("flightpassForm") FlightPassenger flightPassenger,
			@ModelAttribute("addflightForm") FlightPassenger flightPassenger1,
			@RequestParam (value = "search", defaultValue = "") String search,
			@RequestParam(value="sortField", defaultValue = "id") String sortField,
			AddFlight addFlight,
			BindingResult result,
			RedirectAttributes redirectAttributes,
			Model model) {
		System.out.println("Flight Time "+flightPassenger.getAsal());
		flightPassenger.setAsal(flightPassenger.getAsal());
		this.flightPassengerService.save(flightPassenger);
		//this.addFlightService.save(addFlight);
        redirectAttributes.addFlashAttribute("Data Berhasil diinput");
        return "redirect:/flightpassenger";
	}
	
	@GetMapping("/book")
	public String book (@RequestParam("id") Long id, Model model) {
		Optional<FlightPassenger> addflight2 = this.flightPassengerService.getFlightById(id);
		 model.addAttribute("flightpassForm", new FlightPassenger());	
			model.addAttribute("addflightForm", addflight2);
			return "addflightpassenger";

	}

}
