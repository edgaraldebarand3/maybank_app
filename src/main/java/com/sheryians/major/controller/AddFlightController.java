package com.sheryians.major.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.sheryians.major.model.AddFlight;
import com.sheryians.major.service.AddFlightService;
import com.sheryians.major.service.PassangerService;

@Controller
@RequestMapping("/addflight")
public class AddFlightController {
	
	@Autowired 
	AddFlightService addFlightService;
	
	@Autowired
	PassangerService passangerService;
	
	@GetMapping
	public String index (String keyword,
			Model model) {
		List<AddFlight> addflight1 = this.addFlightService.getAll();
		
				
		
		model.addAttribute("addFlightForm", new AddFlight());
		model.addAttribute("addflight1",addflight1);
		
		return "addflight";
	}
	
	@PostMapping("/save")
	public String save (@ModelAttribute("addFlightForm") AddFlight addFlight,
			
			BindingResult result,
			RedirectAttributes redirectAttributes,
			Model model) {
		System.out.println("Flight Time "+addFlight.getFlightTime());
		addFlight.setAirline(addFlight.getAirline());
		this.addFlightService.save(addFlight);
        redirectAttributes.addFlashAttribute("Data Berhasil diinput");
        return "redirect:/addflight";
	}
	
	

}
