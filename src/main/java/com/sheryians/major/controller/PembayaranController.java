package com.sheryians.major.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sheryians.major.helper.Prevalent;
import com.sheryians.major.model.CurrentOnlineUser;
import com.sheryians.major.model.Pembayaran;
import com.sheryians.major.model.PembelianToken;
import com.sheryians.major.model.Rekening;
import com.sheryians.major.model.Tagihan;
import com.sheryians.major.model.Token;
import com.sheryians.major.model.TokenPrice;
import com.sheryians.major.model.User;
import com.sheryians.major.repository.RekeningRepo;
import com.sheryians.major.repository.TagihanRepo;
import com.sheryians.major.service.PembayaranService;
import com.sheryians.major.service.PembelianTokenService;
import com.sheryians.major.service.RekeningService;
import com.sheryians.major.service.TagihanService;
import com.sheryians.major.service.TokenPriceService;
import com.sheryians.major.service.UserService;


@Controller
@RequestMapping("/user/pembayaran")
public class PembayaranController {

	@Autowired
	private PembayaranService pembayaranService;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Autowired
	private TagihanRepo tagihanRepo;
	
	@Autowired
	private TagihanService tagihanService;
	
	@Autowired
	private PembelianTokenService pembelianTokenService;
	
	@Autowired
	private TokenPriceService priceService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	public String index(
			@RequestParam(required = false) String keyword,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			Model model) {
		
		List<TokenPrice> prices = this.priceService.getAll();
		
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser = Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		List<PembelianToken> pembelianTokens = this.pembelianTokenService.findByUser(users.get());
		
		model.addAttribute("keyword", keyword);
		model.addAttribute("pembayaranForm", new Pembayaran());
		model.addAttribute("tagihanForm", new Tagihan());
		model.addAttribute("pembelianTokenForm", new PembelianToken());
		model.addAttribute("page1", pembelianTokens);
		model.addAttribute("listPrice", prices);
		
		return "pembayaran";
	}
	
	@GetMapping("/detail")
	public String getById(@RequestParam("id") Long id, Model model) {

        Optional<Pembayaran> pembayaran = this.pembayaranService.getPembayaranById(id);
        model.addAttribute("pembayaranForm", pembayaran.get());

        
        return "detail-pembayaran";
    }
	
	@GetMapping("/delete")
	public String delete(Pembayaran pembayaran, RedirectAttributes redirectAttributes) {
		this.pembayaranService.delete(pembayaran.getId());
		return "redirect:/user/pembayaran";
	}
	
	@GetMapping("/addToken")
	public String addToken(Model model) {
		model.addAttribute("pembelianTokenForm", new PembelianToken());
		List<TokenPrice> prices = this.priceService.getAll();
		model.addAttribute("listPrice", prices);
		
		return "form_pembeliantoken";
	}
	
	//TOKEN REQUEST
	@PostMapping("/savepembeliantoken")
	public String save(@Valid @ModelAttribute("pembelianTokenForm") PembelianToken pembelianToken,
			@Valid @ModelAttribute("listPrice") TokenPrice tokenPrice, Token token,
			RedirectAttributes redirectAttributes, Model model, BindingResult result) {
		if(result.hasErrors()) {
			List<PembelianToken> pembelianToken1 = this.pembelianTokenService.getAll();
			model.addAttribute("page1",pembelianToken1);
			
			return "pembayaran";
		}
		
		System.out.println(pembelianToken.getNometeran());
		
		//GET DATA USER LOGIN
		CurrentOnlineUser currentUser= Prevalent.currentOnlineUser;
		Optional<User> users = userService.getUserById(currentUser.getId());
		User user = users.get();
				
		//GET DATA REKENING USER
		Rekening rekening = this.rekeningRepo.findByNoRekening(user.getRekening().getNoRekening());
		Double saldoUser = rekening.getSaldo();
				
		
		//MENGURANGI SALDO SETELAH PEMBELIAN
		Double uang = saldoUser - tokenPrice.getHargatoken();
		
		
		//VALIDASI UANG
		if(uang < 50000) {
			redirectAttributes.addFlashAttribute("Fail", "Saldo kurang dari 50000");
			return "redirect:/user/pembayaran";
		}
		
		this.pembelianTokenService.save(pembelianToken, token, tokenPrice);
		redirectAttributes.addFlashAttribute("Success","data inserted");
		return "redirect:/user/pembayaran";
	}
	
	@GetMapping("/detailpembeliantoken")
	public String getByIdPembelianToken(@RequestParam("id") Long id, Model model) {

        Optional<PembelianToken> pembelianToken = this.pembelianTokenService.getPembelianTokenById(id);
        model.addAttribute("pembelianTokenForm", pembelianToken.get());

        
        return "detail-pembelian";
    }
	
	@GetMapping("/deletepembeliantoken")
	public String delete(PembelianToken pembelianToken, RedirectAttributes redirectAttributes) {
		this.pembelianTokenService.delete(pembelianToken.getId());
		return "redirect:/user/pembayaran";
	}
	
}
