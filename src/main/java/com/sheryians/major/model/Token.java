package com.sheryians.major.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Token {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String noToken;
	
	@CreatedDate
	@Column(name="created_date")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date createdDate;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY)
	@JoinColumn(name = "tokenprice_id", referencedColumnName = "id")
	private TokenPrice tokenprices;
	
	@OneToMany(mappedBy = "tokenpembelian")
	private List<PembelianToken> pembeliantoken;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoToken() {
		return noToken;
	}

	public void setNoToken(String noToken) {
		this.noToken = noToken;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public TokenPrice getTokenprices() {
		return tokenprices;
	}

	public void setTokenprices(TokenPrice tokenprices) {
		this.tokenprices = tokenprices;
	}

	public List<PembelianToken> getPembeliantoken() {
		return pembeliantoken;
	}

	public void setPembeliantoken(List<PembelianToken> pembeliantoken) {
		this.pembeliantoken = pembeliantoken;
	}
	
	
}
