package com.sheryians.major.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class TipeTagihan {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@NotBlank
	@NotEmpty
	private String namatipe;
	
	@OneToMany(mappedBy = "tipetagihan")
	private List<Tagihan> listTagihan;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Tagihan> getListTagihan() {
		return listTagihan;
	}

	public void setListTagihan(List<Tagihan> listTagihan) {
		this.listTagihan = listTagihan;
	}

	public String getNamatipe() {
		return namatipe;
	}

	public void setNamatipe(String namatipe) {
		this.namatipe = namatipe;
	}
	
	
	
}
