package com.sheryians.major.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="passanger")

public class Passangers {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String passangerName;
	private String passangerEmail;
	private String passangerContact;
	private String passangerIdentityNumb;
	private String passangerIdentityType;
	private String passangerNationality;
	//private Passangers passangers;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPassangerName() {
		return passangerName;
	}
	public void setPassangerName(String passangerName) {
		this.passangerName = passangerName;
	}
	public String getPassangerEmail() {
		return passangerEmail;
	}
	public void setPassangerEmail(String passangerEmail) {
		this.passangerEmail = passangerEmail;
	}
	public String getPassangerContact() {
		return passangerContact;
	}
	public void setPassangerContact(String passangerContact) {
		this.passangerContact = passangerContact;
	}
	public String getPassangerIdentityNumb() {
		return passangerIdentityNumb;
	}
	public void setPassangerIdentityNumb(String passangerIdentityNumb) {
		this.passangerIdentityNumb = passangerIdentityNumb;
	}
	public String getPassangerNationality() {
		return passangerNationality;
	}
	public void setPassangerNationality(String passangerNationality) {
		this.passangerNationality = passangerNationality;
	}
	public String getPassangerIdentityType() {
		return passangerIdentityType;
	}
	public void setPassangerIdentityType(String passangerIdentityType) {
		this.passangerIdentityType = passangerIdentityType;
	}
	
	
//	public Passangers getPassangers() {
//		return passangers;
//	}
//	public void setPassangers(Passangers passangers) {
//		this.passangers = passangers;
//	}
	
	
	
	

}
