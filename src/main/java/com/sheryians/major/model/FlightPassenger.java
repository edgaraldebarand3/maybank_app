package com.sheryians.major.model;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="flightpassenger")
public class FlightPassenger {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String Asal;
	private String Tujuan;
	private String tanggalKeberangkatan;
	private String seat;

	@ManyToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
	private AddFlight addFlight;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAsal() {
		return Asal;
	}

	public void setAsal(String asal) {
		Asal = asal;
	}

	public String getTujuan() {
		return Tujuan;
	}

	public void setTujuan(String tujuan) {
		Tujuan = tujuan;
	}

	public String getTanggalKeberangkatan() {
		return tanggalKeberangkatan;
	}

	public void setTanggalKeberangkatan(String tanggalKeberangkatan) {
		this.tanggalKeberangkatan = tanggalKeberangkatan;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public AddFlight getAddFlight() {
		return addFlight;
	}

	public void setAddFlight(AddFlight addFlight) {
		this.addFlight = addFlight;
	}

	

}

	
	