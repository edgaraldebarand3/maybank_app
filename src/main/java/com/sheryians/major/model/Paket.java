package com.sheryians.major.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "paket")
public class Paket {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false, unique = true)
	private String kouta;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "paket")
	private List<ProductPaket> list_product;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKouta() {
		return kouta;
	}
	public void setKouta(String kouta) {
		this.kouta = kouta;
	}
	public List<ProductPaket> getList_product() {
		return list_product;
	}
	public void setList_product(List<ProductPaket> list_product) {
		this.list_product = list_product;
	}
	
}
