package com.sheryians.major.model;

import lombok.Data;

@Data
public class CurrentOnlineUser {
	private int id;
	private String firstName;
	private String email;
	private Long rek_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public CurrentOnlineUser(int id, String firstName, String email) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.email = email;
	}
	
	public CurrentOnlineUser(int id, String firstName, String email, Long rek_id) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.email = email;
		this.rek_id = rek_id;
	}
	
	
	
	
}
