package com.sheryians.major.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Tagihan {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	@Column(nullable = false, unique = true)
	private String noPelanggan;
	private Double harga;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY)
	@JoinColumn(name = "tipetagihan_id", referencedColumnName = "id")
	private TipeTagihan tipetagihan;
	
	@OneToMany(mappedBy = "tagihanuser")
	private List<Pembayaran> pembayaran;
	
	Boolean paid = false;
	
	public List<Pembayaran> getPembayaran() {
		return pembayaran;
	}
	public void setPembayaran(List<Pembayaran> pembayaran) {
		this.pembayaran = pembayaran;
	}
	public Boolean getPaid() {
		return paid;
	}
	public void setPaid(Boolean paid) {
		this.paid = paid;
	}
	public TipeTagihan getTipetagihan() {
		return tipetagihan;
	}
	public void setTipetagihan(TipeTagihan tipetagihan) {
		this.tipetagihan = tipetagihan;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNoPelanggan() {
		return noPelanggan;
	}
	public void setNoPelanggan(String noPelanggan) {
		this.noPelanggan = noPelanggan;
	}
	public Double getHarga() {
		return harga;
	}
	public void setHarga(Double harga) {
		this.harga = harga;
	}
	
	
}
