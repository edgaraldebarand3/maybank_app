package com.sheryians.major.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class PembelianToken {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	@CreatedDate
	@Column(name="tanggal_pembelian")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tanggalPembelian;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY)
	@JoinColumn(name = "token_id", referencedColumnName = "id")
	private Token tokenpembelian;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	private String nometeran;

	public String getNometeran() {
		return nometeran;
	}

	public void setNometeran(String nometeran) {
		this.nometeran = nometeran;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTanggalPembelian() {
		return tanggalPembelian;
	}

	public void setTanggalPembelian(Date tanggalPembelian) {
		this.tanggalPembelian = tanggalPembelian;
	}

	public Token getTokenpembelian() {
		return tokenpembelian;
	}

	public void setTokenpembelian(Token tokenpembelian) {
		this.tokenpembelian = tokenpembelian;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	};
	
	
}
