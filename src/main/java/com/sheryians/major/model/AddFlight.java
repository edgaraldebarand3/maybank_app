package com.sheryians.major.model;

import java.sql.Time;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="flightschedule")
public class AddFlight {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String flightCode;
	private String Category;
	private String Airline;
	private String Asal;
	private String Tujuan;
	private String tanggalKeberangkatan;
	private Double harga;
	private String flightTime;
	private String seat;
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "addFlight",cascade = CascadeType.ALL)
	private List<FlightPassenger> listflightpass;	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFlightCode() {
		return flightCode;
	}
	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getAirline() {
		return Airline;
	}
	public void setAirline(String airline) {
		Airline = airline;
	}
	public String getAsal() {
		return Asal;
	}
	public void setAsal(String asal) {
		Asal = asal;
	}
	public String getTujuan() {
		return Tujuan;
	}
	public void setTujuan(String tujuan) {
		Tujuan = tujuan;
	}
	public String getTanggalKeberangkatan() {
		return tanggalKeberangkatan;
	}
	public void setTanggalKeberangkatan(String tanggalKeberangkatan) {
		this.tanggalKeberangkatan = tanggalKeberangkatan;
	}
	public Double getHarga() {
		return harga;
	}
	public void setHarga(Double harga) {
		this.harga = harga;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getFlightTime() {
		return flightTime;
	}
	public void setFlightTime(String flightTime) {
		this.flightTime = flightTime;
	}
	public List<FlightPassenger> getListflightpass() {
		return listflightpass;
	}
	public void setListflightpass(List<FlightPassenger> listflightpass) {
		this.listflightpass = listflightpass;
	}


	
	
	

}
