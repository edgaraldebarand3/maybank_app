package com.sheryians.major.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class TokenPrice {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Double hargatoken;
	
	@OneToMany(mappedBy = "tokenprices")
	private List<Token> token;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getHargatoken() {
		return hargatoken;
	}

	public void setHargatoken(Double hargatoken) {
		this.hargatoken = hargatoken;
	}
	
	
}
