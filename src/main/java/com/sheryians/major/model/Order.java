package com.sheryians.major.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Column(name = "first_name")
	private String firstName;
	
	  @Column(name = "last_name") 
	  private String lastName;
	 
	private String address;
	@Column(name = "post_code")
	private String postcode;
	private String city;
	private String phone;
	private String email;
	private String information;
	private double carts;
	@ManyToOne
	@JoinColumn
	private User user;

}
