package com.sheryians.major.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Pembayaran {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@CreatedDate
	@Column(name="tanggal_pembayaran")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tanggalPembayaran;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY)
	@JoinColumn(name = "tagihan_id", referencedColumnName = "id")
	private Tagihan tagihanuser;

	public Tagihan getTagihanuser() {
		return tagihanuser;
	}

	public void setTagihanuser(Tagihan tagihanuser) {
		this.tagihanuser = tagihanuser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTanggalPembayaran() {
		return tanggalPembayaran;
	}

	public void setTanggalPembayaran(Date tanggalPembayaran) {
		this.tanggalPembayaran = tanggalPembayaran;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
